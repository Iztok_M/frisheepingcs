Unity simulation of a shepherding algorithm using the Compute Shaders. With the use of the Compute Shaders we have achieved that instead of doing computations on the CPU (central processing unit), computations are done on the GPU (graphics processing unit). This way computations are done in parallel and our simulation works with a larger number of agents. However, we have encountered a bug in our code which we are unable to identify, therefore our simulation does not work as intended.

To run the simulation, first clone the repository to your locan disk and then press Run in Unity. Change "N Of Sheep" parameter in GameManager to get a simulation for a different number of sheep.

Unity version: 2019.4.12f1